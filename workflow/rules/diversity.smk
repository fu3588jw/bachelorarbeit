rule diversity:
    input:
        tables = "results/tax_profiling/tables/{tool}_table.tsv",
        sample_table = "resources/sample_data_.txt"
    output:
        alpha = "results/diversity/{tool}/alpha_diversity.txt",
        alpha_plot = "results/diversity/{tool}/alpha_plot.png",
        beta_plot = "results/diversity/{tool}/beta_plot.png"
    params:
        seq = config["sample_data"]["sample_names"],
        group = config["sample_data"]["sample_groups"]
    log:
        "results/logs/diversity/{tool}.log"
    conda:
        "../envs/diversity.yaml"
    script:
        "../scripts/diversity.R"

rule diversity_filtered:
    input:
        tables = "results/tax_profiling/tables/filtered/{tool}_table.tsv",
        sample_table = "resources/sample_data_.txt",
        alpha = "results/diversity/{tool}/alpha_diversity.txt"
    output:
        beta_plot = "results/diversity/{tool}/beta_plot_filtered.png"
    params:
        seq = config["sample_data"]["sample_names"],
        group = config["sample_data"]["sample_groups"]
    log:
        "results/logs/diversity/filtered/{tool}.log"
    conda:
        "../envs/diversity.yaml"
    script:
        "../scripts/diversity_f.R"