rule trim_fastq: 
    input:
        fasta_file1 = lambda wildcards: samples.at[wildcards.sample,'fq1'] if 
        wildcards.sample in samples.index else '',
        fasta_file2 = lambda wildcards: samples.at[wildcards.sample,'fq2'] if 
        wildcards.sample in samples.index else ''
    output:
        "results/processing/fastq/trimmed/{sample}_1.fastq.gz",
        "results/processing/fastq/orphan/{sample}_1.fastq.gz",
        "results/processing/fastq/trimmed/{sample}_2.fastq.gz",
        "results/processing/fastq/orphan/{sample}_2.fastq.gz"
    params: 
        illumina = config["trimmomatic"]["ILLUMINACLIP"]["adapters"],
        clip = config["trimmomatic"]["ILLUMINACLIP"]["clip_opts"],
        leading = config["trimmomatic"]["LEADING"],
        trailing = config["trimmomatic"]["TRAILING"],
        minlen = config["trimmomatic"]["MINLEN"],
        slidingwindow = config["trimmomatic"]["SLIDINGWINDOW"]
    log:
        "results/logs/trimmed_fastq/{sample}.log"
    threads: 4
    conda:
        "../envs/process.yaml"
    shell:
        "trimmomatic PE -threads {threads} -phred33 {input.fasta_file1} {input.fasta_file2} {output} ILLUMINACLIP:{params.illumina}:{params.clip} LEADING:{params.leading} TRAILING:{params.trailing} SLIDINGWINDOW:{params.slidingwindow} MINLEN:{params.minlen} 2> {log}"


rule create_index_bowtie2: 
    input:
        reference = config["reference"]
    output:
        expand(config["bowtie"]["b_index"]+"{ref}", ref = [".1.bt2", ".2.bt2", ".3.bt2", ".4.bt2", ".rev.1.bt2", ".rev.2.bt2"])
    log: 
        "results/logs/index/index_bowtie2.log"
    params:
        bowtie_index = config["bowtie"]["b_index"]
    threads: 4
    conda: 
        "../envs/process.yaml"
    shell:
        "bowtie2-build --threads {threads} {input.reference} {params.bowtie_index} 2> {log}"


rule create_alignment:
    input:
        fasta1 = "results/processing/fastq/trimmed/{sample}_1.fastq.gz",
        fasta2 = "results/processing/fastq/trimmed/{sample}_2.fastq.gz",
        refe = expand(config["bowtie"]["b_index"]+"{ref}", ref = [".1.bt2", ".2.bt2", ".3.bt2", ".4.bt2", ".rev.1.bt2", ".rev.2.bt2"])
    output:
        "results/processing/samfiles/sam/{sample}.sam"
    params:
        index_bowtie = config["bowtie"]["b_index"],
        N = config["bowtie"]["max_mismatches"],
        L = config["bowtie"]["length_seed_substrings"],
        extra = config["bowtie"]["extra"]
    log:
        "results/logs/samfiles/{sample}.log"
    threads: 4
    conda:
        "../envs/process.yaml"
    shell:
        "bowtie2 --threads {threads} -N {params.N} -L {params.L} {params.extra} -x {params.index_bowtie} -1 {input.fasta1} -2 {input.fasta2} -S {output} 2> {log}"


rule sort_bam: 
    input:
        "results/processing/samfiles/sam/{sample}.sam"
    output: 
        "results/processing/samfiles/bam/{sample}.sorted.bam"
    log: 
        "results/logs/bam_sorted/{sample}.log"
    conda: 
        "../envs/process.yaml"
    shell:
        "samtools sort -o {output} -n {input} 2> {log}"


rule samtools_fastq_separate:
    input:
        "results/processing/samfiles/bam/{sample}.sorted.bam"
    output:
        fq_gz1 = "results/processing/fastq/processed/{sample}_1.fq.gz",
        fq_gz2 = "results/processing/fastq/processed/{sample}_2.fq.gz"
    log:
        "results/logs/convert_fastq/{sample}_1.log"
    params:
        host_filtering = config["host_filter"]
    conda: 
        "../envs/process.yaml"
    threads: 4
    shell:
        "samtools fastq -@ {threads} {input} -1 {output.fq_gz1} -2 {output.fq_gz2} -0 /dev/null -s /dev/null -n -f {params.host_filtering} 2> {log}"
