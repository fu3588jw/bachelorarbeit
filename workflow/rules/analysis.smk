rule deseq2:
    input:
        tables = "results/tax_profiling/tables/filtered/{tool}_table.tsv",
        sample_table = config["sample_data"]["path"]
    output:
        significant_results = "results/analysis/deseq2/{tool}/significant_results.txt",
        all_results = "results/analysis/deseq2/{tool}/all_results.txt",
        MA_plot = "results/analysis/deseq2/{tool}/ma_plot.png",
        PCA_plot = "results/analysis/deseq2/{tool}/pca_plot.png"
    params:
        seq = config["sample_data"]["sample_names"],
        groups = config["sample_data"]["sample_groups"],
        group1 = config["sample_data"]["group_1"],
        group2 = config["sample_data"]["group_2"],
        sig_wert = config["analysis"]["signifikanzwert"]
    log:
        "results/logs/analysis/deseq2/{tool}.log"
    conda:
        "../envs/diversity.yaml"
    script:
        "../scripts/deseq.R"

#rule Maaslin2:
#    input:
#        tables = "results/tax_profiling/tables/filtered/{tool}_table.tsv",
#        sample_table = config["sample_data"]["path"],
#        maaslin_R = "resources/Maaslin/Maaslin2-master/R/Maaslin2.R"
#    output:
#        all_results = "results/analysis/maaslin2/{tool}/all_results.tsv",
#        significant_results = "results/analysis/maaslin2/{tool}/significant_results.tsv"
#    params: 
#        fixed = config["analysis"]["fixed_effects"],
#        ref_gs = config["analysis"]["group_ref"]
#    log:
#        "results/logs/analysis/maaslin2/{tool}.log"
#    conda:
#        "../envs/maaslin.yaml"
#    shell: 
#        "Rscript {input.maaslin_R} --fixed_effects={params.fixed} --reference={params.ref_gs} --standardize=FALSE {input.tables} {input.sample_table} results/analysis/maaslin2/{wildcards.tool}"

rule maaslin_filter:
    input:
        all_results = "results/analysis/maaslin2/{tool}/all_results.tsv"
    output:
        significant_results_filtered = "results/analysis/maaslin2/{tool}/significant_results_filter.tsv"
    params: 
        groups = config["sample_data"]["sample_groups"],
        group2 = config["sample_data"]["group_2"],
        sig_wert = config["analysis"]["signifikanzwert"]
    log:
        "results/logs/analysis/maaslin2/filter/{tool}.log"
    conda:
        "../envs/filtering.yaml"
    script:
        "../scripts/filter.R"

#rule ancombc2:
#    input:
#        tables = "results/tax_profiling/tables/filtered/{tool}_table.tsv",
#        sample_table = config["sample_data"]["path"]
#    output:
#        all_results = "results/analysis/ancom/{tool}/all_results.tsv",
#        significant_results = "results/analysis/ancom/{tool}/significant_results.tsv"
#    params:
#        seq = config["sample_data"]["sample_names"],
#        groups = config["sample_data"]["sample_groups"],
#        sig_wert = config["analysis"]["signifikanzwert"]
#    log:
#        "results/logs/analysis/ancom/{tool}.log"
#    conda:
#        "../envs/filtering.yaml"
#    script:
#        "../scripts/ancom.R"

rule compare:
    input:
        maaslin_results = "results/analysis/maaslin2/{tool}/significant_results_filter.tsv",
        deseq_results = "results/analysis/deseq2/{tool}/significant_results.txt",
        ancom_results = "results/analysis/ancom/{tool}/significant_results.tsv"
    output:
        venn_diagramm = "results/analysis/benchmarking/{tool}_venn_diagramm.png"
    log:
        "results/logs/analysis/benchmarking/{tool}.log"
    conda:
        "../envs/benchmarking.yaml"
    script:
        "../scripts/benchmarking.R"

rule compare_tax:
    input:
        bracken = "results/tax_profiling/tables/filtered/bracken_table.tsv",
        clark = "results/tax_profiling/tables/filtered/clark_table.tsv",
        metaphlan = "results/tax_profiling/tables/filtered/metaphlan_table.tsv",
        ganon = "results/tax_profiling/tables/filtered/ganon_table.tsv"
    output:
        venn_diagramm = "results/analysis/benchmarking/tax_venn_diagramm.png"
    log:
        "results/logs/analysis/benchmarking/tax.log"
    conda:
        "../envs/benchmarking.yaml"
    script:
        "../scripts/benchmarking_tax.R" 