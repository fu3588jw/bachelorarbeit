rule kraken:
    input:
        fq_g1 = "results/processing/fastq/processed/{sample}_1.fq.gz",
        fq_g2 = "results/processing/fastq/processed/{sample}_2.fq.gz"
    output:
        kraken_op = "results/tax_profiling/braken/kraken/output/{sample}.kraken.txt",
        kraken_rp =  "results/tax_profiling/braken/kraken/report/{sample}.kreport.txt"
    log:
        "results/logs/kraken/report/{sample}_kraken.log"
    params:
        database = config["databases"]["Kraken_database"]
    threads: 6
    conda:
        "../envs/taxonomic.yaml"
    shell:
        "kraken2 --threads {threads} --db {params.database} --output {output.kraken_op} --report {output.kraken_rp} --fastq-input --gzip-compressed --paired {input.fq_g1} {input.fq_g2} 2> {log}"


rule bracken:
    input:
        "results/tax_profiling/braken/kraken/report/{sample}.kreport.txt"
    output:
        bracken_output = "results/tax_profiling/braken/braken/output/{sample}.kreport.txt",
        bracken_report = "results/tax_profiling/braken/braken/report/{sample}_.txt"
    log:
        "results/logs/bracken/{sample}.log"
    params:
        database = config["databases"]["Kraken_database"],
        READ_LEN = config["bracken"]["reads"],
        CLASSIFICATION_LEVEL = config["bracken"]["classification"],
        THRESHOLD = config["bracken"]["threshold"],
    conda:
        "../envs/taxonomic.yaml"
    shell:
        "bracken -d {params.database} -i {input} -o {output.bracken_output} -w {output.bracken_report} -r {params.READ_LEN} -l {params.CLASSIFICATION_LEVEL} -t {params.THRESHOLD} 2> {log}"

rule create_biom: 
    input:
        expand("results/tax_profiling/braken/braken/report/{sample}_.txt", sample=list(samples.index))
    output:
        braken_tbl_raw="results/tax_profiling/braken/table_raw.biom"
    log:
        "results/logs/kraken_biom/table_raw.log"
    conda:
        "../envs/taxonomic.yaml"
    shell:
        "kraken-biom {input} -o {output} --fmt json 2> {log}"

rule edit_braken_table:
    input:
        braken_table_raw="results/tax_profiling/braken/table_raw.biom"
    output:
        braken_tbl="results/tax_profiling/tables/bracken_table.tsv",
        braken_tbl_filtered="results/tax_profiling/tables/filtered/bracken_table.tsv"
    log:
        "results/logs/kraken_biom/table_edt.log"
    params:
        min_anundance=config["tax_filter"]["min_abundance"],
        percent=config["tax_filter"]["percent"]
    conda:
        "../envs/diversity.yaml"
    script:
        "../scripts/edit_braken.R"