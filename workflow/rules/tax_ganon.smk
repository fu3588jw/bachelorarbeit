rule ganon:
    input:
        fq_g1 = "results/processing/fastq/unzipped/{sample}_1.fq",
        fq_g2 = "results/processing/fastq/unzipped/{sample}_2.fq"
    output:
        ganon_op = "results/tax_profiling/ganon/test/{sample}_.tre",
        ganon_rp = "results/tax_profiling/ganon/test/{sample}_.rep"
    log:
        "results/logs/ganon/{sample}_ganon.log"
    params:
        database = config["databases"]["Ganon_database"]
    conda:
        "../envs/taxonomic.yaml"
    shell:
        "ganon classify --db-prefix {params.database} --output-prefix results/tax_profiling/ganon/test/{wildcards.sample}_ --paired-reads {input.fq_g1} {input.fq_g2} --threads 64 2> {log}"

rule create_ganon_table:
    input:
        tre_files=expand("results/tax_profiling/ganon/output/{sample}_.tre", sample=list(samples.index))
    output:
        ganon_tbl= "results/tax_profiling/ganon/table_raw.tsv"
    log:
        "results/logs/ganon/table/table_raw.log"
    conda:
        "../envs/taxonomic.yaml"
    shell:
        "ganon table --input {input.tre_files} --output-file {output} --rank species"

rule edit_ganon_table:
    input:
        ganon_table_raw="results/tax_profiling/ganon/table_raw.tsv",
        database="/buffer/ag_bsc/pmsb_workflows_2023/meta_tax/bachelorarbeit/resources/Database/Ganon_DB/arc_bac.tax"
    output:
        ganon_tbl="results/tax_profiling/tables/ganon_table.tsv",
        ganon_tbl_filtered="results/tax_profiling/tables/filtered/ganon_table.tsv"
    log:
        "results/logs/ganon/table/table_edt.log"
    params:
        min_anundance=config["tax_filter"]["min_abundance"],
        percent=config["tax_filter"]["percent"]
    conda:
        "../envs/diversity.yaml"
    script:
        "../scripts/edit_ganon.R"