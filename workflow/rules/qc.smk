rule raw_fastqc:
    input:
        lambda wildcards: samples.at[wildcards.sample,f"fq{int(wildcards.idx)}"]
    output:
        html="results/processing/fastqc/raw_data/{sample}_{idx}.html",
        zip="results/processing/fastqc/raw_data/{sample}_{idx}_fastqc.zip"
    params: "--quiet"
    log:
        "results/logs/raw_data/{sample}_{idx}.log"
    threads: 1
    conda: 
        "../envs/process.yaml"
    wrapper:
        "v1.25.0/bio/fastqc"
        
rule multiqc_raw:
    input:
        expand("results/processing/fastqc/raw_data/{sample}_{idx}.html", sample=list(samples.index), idx = ["1", "2"]),
        expand("results/processing/fastqc/raw_data/{sample}_{idx}_fastqc.zip", sample=list(samples.index), idx = ["1", "2"])
    output:
        "results/processing/multiqc/raw_report.html"
    params:
        extra="", 
        use_input_files_only=True, 
    log:
        "results/logs/multiqc/raw/report.log"
    wrapper:
        "v1.30.0/bio/multiqc"


rule trimm_fastqc:
    input:
        "results/processing/fastq/trimmed/{sample}_{idx}.fastq.gz"
    output:
        html="results/processing/fastqc/trimm_data/{sample}_{idx}.html",
        zip="results/processing/fastqc/trimm_data/{sample}_{idx}_fastqc.zip"
    params: "--quiet"
    log:
        "results/logs/trimm_fastqc/{sample}_{idx}.log"
    threads: 4
    conda: 
        "../envs/process.yaml"
    wrapper:
        "v1.25.0/bio/fastqc"


rule multiqc_trimmed:
    input:
        expand("results/processing/fastqc/raw_data/{sample}_{idx}.html", sample=list(samples.index), idx = ["1", "2"]),
        expand("results/processing/fastqc/trimm_data/{sample}_{idx}.html", sample=list(samples.index), idx = ["1", "2"])
    output:
        "results/processing/multiqc/trimmed_report.html"
    params:
        extra=""
    log:
        "results/logs/multiqc/trimmed/report.log"
    wrapper:
        "v1.30.0/bio/multiqc"