rule unzip:
    input: 
        fq = "results/processing/fastq/processed/{sample}_{idx}.fq.gz"
    output: 
        ofq = "results/processing/fastq/unzipped/{sample}_{idx}.fq"
    log:
        "results/logs/processing/fastq/unzipped/{sample}_{idx}.log"
    shell: 
        "gzip -dkc < {input.fq} > {output.ofq}"

rule clark_op:
    input:
        fq_g1 = "results/processing/fastq/unzipped/{sample}_1.fq",
        fq_g2 = "results/processing/fastq/unzipped/{sample}_2.fq"
    output:
        clark_op="results/tax_profiling/clark/output/{sample}_.csv"
    log:
        "results/logs/clark/{sample}_clark.log"
    params:
        pfad = config["clark"]["pfad"]
    conda:
        "../envs/taxonomic.yaml"
    shell:
        "{params.pfad}/classify_metagenome.sh -P {input.fq_g1} {input.fq_g2} -R results/tax_profiling/clark/output/{wildcards.sample}_ 2> {log}"


rule clark_rp:
    input:
        clark_op="results/tax_profiling/clark/output/{sample}_.csv"
    output:
        clark_op2="results/tax_profiling/clark/output2/{sample}_.txt"
    log:
        "results/logs/clark/output2/{sample}_clark.log"
    params:
        database = config["databases"]["Clark_database"],
        pfad = config["clark"]["pfad"]
    conda:
        "../envs/taxonomic.yaml"
    shell:
        "{params.pfad}/estimate_abundance.sh -F {input.clark_op} -D {params.database} > {output.clark_op2}"

rule clark_tables:
    input:
        clark_op2=expand("results/tax_profiling/clark/output2/{sample}_.txt", sample=list(samples.index))
    output:
        Table_per_Report="results/tax_profiling/clark/TableSummary_per_Report.csv",
        Table_HitCount="results/tax_profiling/clark/TableSummary_HitCount.csv",
        Table_per_Taxon="results/tax_profiling/clark/TableSummary_per_Taxon.csv"
    params:
        pfad = config["clark"]["pfad"],
        top_r = config["clark"]["top"],
        min_abundance = config["clark"]["min_abundance"]
    conda:
        "../envs/taxonomic.yaml"
    shell:
        "{params.pfad}/makeSummaryTables.sh {params.top_r} {params.min_abundance} {input.clark_op2}"

rule edit_clark_table:
    input:
        clark_table_raw="results/tax_profiling/clark/TableSummary_HitCount.csv"
    output:
        clark_tbl= "results/tax_profiling/tables/clark_table.tsv",
        clark_tbl_filtered= "results/tax_profiling/tables/filtered/clark_table.tsv"
    log:
        "results/logs/clark/table_clark_edt.log"
    params:
        min_anundance=config["tax_filter"]["min_abundance"],
        percent=config["tax_filter"]["percent"]
    conda:
        "../envs/diversity.yaml"
    script:
        "../scripts/edit_clark.R"