rule taxonomic_metaphlan:
    input:
        fq_g1 = "results/processing/fastq/processed/{sample}_1.fq.gz",
        fq_g2 = "results/processing/fastq/processed/{sample}_2.fq.gz"
    output:
        metaphlan_op="results/tax_profiling/metaphlan/output/{sample}_.txt",
        bwt = "results/tax_profiling/metaphlan/output/{sample}_bowtie2.bz2"
    params:
        database = config["databases"]["Metaphlan_database"]
    log:
        "results/logs/tax_profiling/metaphlan/{sample}_output.log"
    conda:
        "../envs/taxonomic.yaml"
    shell:
        "metaphlan {input.fq_g1},{input.fq_g2} --bowtie2out {output.bwt} --bowtie2db {params.database} --input_type fastq -t rel_ab_w_read_stats -o {output.metaphlan_op}"

rule merge_metaphlan_tables:
    input:
        txt_files=expand("results/tax_profiling/metaphlan/output/{sample}_.txt", sample=list(samples.index))
    output:
        metaphlan_tbl_raw="results/tax_profiling/metaphlan/table_raw.txt"
    log:
        "results/logs/tax_profiling/metaphlan/table_raw.log"
    conda:
        "../envs/taxonomic.yaml"
    shell:
        "/buffer/ag_bsc/pmsb_workflows_2023/meta_tax/bachelorarbeit/workflow/scripts/merge_metaphlan_tables_abs.py {input.txt_files} > {output}"


rule edit_metaphlan_table:
    input:
        metaphlan_tbl_raw="results/tax_profiling/metaphlan/table_raw.txt"
    output:
        metaphlan_tbl="results/tax_profiling/tables/metaphlan_table.tsv",
        metaphlan_tbl_filtered="results/tax_profiling/tables/filtered/metaphlan_table.tsv"
    log:
        "results/logs/tax_profiling/metaphlan/table_edt.log"
    params:
        min_anundance=config["tax_filter"]["min_abundance"],
        percent=config["tax_filter"]["percent"]
    conda:
        "../envs/diversity.yaml"
    script:
        "../scripts/edit_metaphlan.R"

