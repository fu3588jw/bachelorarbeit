library(dplyr)
library(VennDiagram)

maaslin <- read.table(snakemake@input[["maaslin_results"]], sep = "\t", header = TRUE, row.names = "feature")
deseq <- read.table(snakemake@input[["deseq_results"]], sep = "\t", header = TRUE, row.names = 1)
ancom <- read.table(snakemake@input[["ancom_results"]], sep = "\t", header = TRUE, row.names = "taxon")

maalin_tax <- row.names(maaslin)
deseq_tax <- row.names(deseq)
ancom_tax <- row.names(ancom)

venn.diagram(x = list(Maaslin2 = maalin_tax, Deseq2 = deseq_tax, Ancom = ancom_tax),
             filename = snakemake@output[["venn_diagramm"]],
             fill = c("blue", "red", "green"))
