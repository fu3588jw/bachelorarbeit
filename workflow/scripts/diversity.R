library(phyloseq)
library(vegan)
library(ggplot2)

tax_tab <- read.table(snakemake@input[["tables"]], sep = "\t", header = TRUE)
samples <- read.csv(snakemake@input[["sample_table"]], sep = "\t", header = TRUE, row.names = snakemake@params[["seq"]])

my_phyloseq <- phyloseq(otu_table(tax_tab, taxa_are_rows = TRUE), sample_data(samples))

alpha_diversity <- estimate_richness(my_phyloseq)

write.table(alpha_diversity, file = snakemake@output[["alpha"]], sep = "\t", quote = FALSE, row.names = TRUE, col.names = TRUE)

png(snakemake@output[["alpha_plot"]])
plot_richness(my_phyloseq, color = snakemake@params[["group"]])
dev.off()

pcoa_bc = ordinate(my_phyloseq, "PCoA", "bray")

png(snakemake@output[["beta_plot"]])
plot_ordination(my_phyloseq, pcoa_bc, color = snakemake@params[["group"]])
dev.off()